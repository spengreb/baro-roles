import random
from random import choice
from collections import OrderedDict
roles = { "Captain": [0,1], "Medic": [0,1], "Mechanical": [0,1], "Electrical": [0,2], "Security": [0,1], "Assistant": [0,2] }
personnel = ["Ulven", "Scootz", "Nyx", "Ojorn", "Slux", "Jaoder", "Promo"]
picked_role = [None] * len(personnel)

def main():
    for indx, person in enumerate(personnel):
        picked_role = pick_a_role(indx, person)
        # Log the fact we picked a roll. Increment roll amount to 1 (probably another function to iterate through roles and update tuple)
        print(f'{person} is {picked_role}')     

def pick_a_role(indx, person):
    picked_role = get_random_role(indx)

    if (picked_role_is_valid(picked_role)):
        return picked_role
    else:
        # print(f"TOO MANY ENROLLED AS {picked_role}, REROLLING FOR USER {person}")
        return pick_a_role(indx, person)

def get_random_role(indx):
    shuffled_roles = list(roles.items())
    random.shuffle(shuffled_roles)
    role = choice(list(roles.keys()))
    picked_role[indx] = role

    return picked_role[indx]

def picked_role_is_valid(picked_role):
    # #PRINT ROLE NAME
    # print(picked_role)
    # #PRINT ROLE LIST VALUES
    # print(roles[picked_role])

    role_value = roles[picked_role]
    if (role_value[0]) < role_value[1]:
        role_value[0] += 1
        #print("Valid Role")
        return True
    else:
        return False


main()